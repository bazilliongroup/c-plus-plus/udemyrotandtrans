import numpy as np

#rotation vector
def rotationX(theta):
    return np.array([[1,0,0],
                     [0, np.cos(theta), np.sin(theta)],
                     [0, -np.sin(theta), np.cos(theta)]])

# 30 degree rotation
Rx = rotationX(30.0*(np.pi/180.0))
print("30 degree rotation")
print("Rx {}".format(Rx))

#regular vector
x_a = np.array([0.7, 1.2, -0.3])
print("regular vector x_a")
print("x_a {}".format(x_a))

#matrix multiply to do a 30 degre rotation of x_a 
x_b = np.matmul(Rx, x_a)
print("matrix multiply to do a 30 degree rotation of x_a to be x_b")
print("x_b {}".format(x_b))

