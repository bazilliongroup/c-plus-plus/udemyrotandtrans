import numpy as np

def rotationX(theta):
    return np.array([[1,          0,           0],
                     [0,  np.cos(theta),  np.sin(theta)],
                     [0,  -np.sin(theta),  np.cos(theta)]])

def rotationY(theta):
    return np.array([[np.cos(theta),  0,  -np.sin(theta)],
                     [0,              1,       0],
                     [np.sin(theta),  0,  np.cos(theta)]])

def rotationZ(theta):
    return np.array([[np.cos(theta),  np.sin(theta),  0],
                     [-np.sin(theta),  np.cos(theta),  0],
                     [0,                    0,        1]])

# Here we rotate from the euler angle Rotations to XYZ Sequence
def rotationEulerXYZ(attitude_xyz):
    Rx = rotationX(attitude_xyz[0])
    Ry = rotationY(attitude_xyz[1])
    Rz = rotationZ(attitude_xyz[2])
    R = np.matmul(Rx, np.matmul(Ry, Rz))
    return R                     

def radToDeg(rad):
    return np.array(rad) * (180.0 / np.pi)

def degToRad(deg):
    return np.array(deg) * (np.pi / 180.0)

def printEulerAngles(title, attitude):
    print("Euler Angles {} [{}, {}, {}]".format(title, attitude[0], attitude[1], attitude[2]))

def linearInterpolate(R0, R1, t):
    return (R0 * (1-t) + R1 * t)

attitude0 = degToRad([0, 0, 0])
attitude1 = degToRad([-30, 45, 135])

printEulerAngles("Start", radToDeg(attitude0))
printEulerAngles("End", radToDeg(attitude1))

attitude_interp = linearInterpolate(attitude0, attitude1, 0.5)
printEulerAngles("Interp t=0.5", radToDeg(attitude_interp))