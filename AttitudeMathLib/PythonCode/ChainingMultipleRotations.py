import numpy as np

def rotationX(theta):
    return np.array([[1,0,0],
                    [0, np.cos(theta), np.sin(theta)],
                    [0, -np.sin(theta), np.cos(theta)]])

#regular vector x_a
x_a = np.array([0, 1, 0])
print("regular vector x_a")
print("x_a {}".format(x_a))

#rotation from the a frame to the b frame 
R_ba = rotationX(30.0*(np.pi/180.0))
#rotation from the b frame to the c frame
R_cb = rotationX(15.0*(np.pi/180.0))

#apply the matricx multiplication of a frame into b frame
x_b = np.matmul(R_ba, x_a)
print("apply the matrix multiplication of x_a frame vector into b frame")
print("x_b {}".format(x_b))

#apply the matricx multiplication of b frame into c frame
x_c = np.matmul(R_cb, x_b)
print("apply the matricx multiplication of x_b frame into c frame")
print("x_c {}".format(x_c))

#matrix multply rotations from the a frame to the c frame 
R_ca = np.matmul(R_cb, R_ba)
print("matrix multply rotations from the R_cb and R_ba to the R_ca frame")
print("R_ca {}".format(R_ca))

# 45 degree rotation
x_c2 = np.matmul(R_ca, x_a)
print("45 degree rotation")
print("x_c2 {}".format(x_c2))
# verify x_c2 as a 45 degree rotation
R = rotationX(45.0*(np.pi/180.0))
print("verify x_c2 as a 45 degree rotation")
print("R {}".format(R))