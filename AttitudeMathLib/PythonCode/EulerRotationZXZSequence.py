import numpy as np

def rotationX(theta):
    return np.array([[1,          0,           0],
                     [0,  np.cos(theta),  np.sin(theta)],
                     [0,  -np.sin(theta),  np.cos(theta)]])

def rotationY(theta):
    return np.array([[np.cos(theta),  0,  -np.sin(theta)],
                     [0,              1,       0],
                     [np.sin(theta),  0,  np.cos(theta)]])

def rotationZ(theta):
    return np.array([[np.cos(theta),  np.sin(theta),  0],
                     [-np.sin(theta),  np.cos(theta),  0],
                     [0,                    0,        1]])

def radToDeg(rad):
    return rad * (180.0 / np.pi)

def degToRad(deg):
    return deg * (np.pi / 180.0)

def rotationEulerZXZ(phi, theta, psi):
    Rz2 = rotationZ(phi)
    Rx = rotationX(theta)
    Rz1 = rotationZ(psi)
    R = np.matmul(Rz2, np.matmul(Rx, Rz1))
    return R

#degrees
phi_deg = -30.0    #roll
theta_deg = 65.0  #pitch
psi_deg = -45.0   #yaw

print("Euler Angles Degrees[{}, {}, {}]".format(phi_deg, theta_deg, psi_deg))

phi = degToRad(phi_deg)
theta = degToRad(theta_deg)
psi = degToRad(psi_deg)
print("Euler Angles Radians[{}, {}, {}]".format(phi, theta, psi))

Rzxz = rotationEulerZXZ(phi, theta, psi)
print("Euler Angles Rzxz\n {}".format(Rzxz))

def eulerAnglesFromRzxz(Rzxz):
    phi = np.arctan2(Rzxz[0][2], Rzxz[1][2])
    theta = np.arccos(Rzxz[2][2])
    psi = np.arctan2(Rzxz[2][0], -Rzxz[2][1])
    return (phi, theta, psi)

attitude = eulerAnglesFromRzxz(Rzxz)
phi2_deg = radToDeg(attitude[0])
theta2_deg = radToDeg(attitude[1])
psi2_deg = radToDeg(attitude[2])
print("Recovered Euler Angles Radians [{}, {}, {}]".format(attitude[0], attitude[1], attitude[2]))
print("Recovered Euler Angles Degrees [{}, {}, {}]".format(phi2_deg, theta2_deg, psi2_deg))