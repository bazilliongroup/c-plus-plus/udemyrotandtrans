import numpy as np

def rotationX(theta):
    return np.array([[1,          0,           0],
                     [0,  np.cos(theta),  np.sin(theta)],
                     [0,  -np.sin(theta),  np.cos(theta)]])

def rotationY(theta):
    return np.array([[np.cos(theta),  0,  -np.sin(theta)],
                     [0,              1,       0],
                     [np.sin(theta),  0,  np.cos(theta)]])

def rotationZ(theta):
    return np.array([[np.cos(theta),  np.sin(theta),  0],
                     [-np.sin(theta),  np.cos(theta),  0],
                     [0,                    0,        1]])

def radToDeg(rad):
    return rad * (180.0 / np.pi)

def degToRad(deg):
    return deg * (np.pi / 180.0)

# Here we rotate from the euler angle Rotations to XYZ Sequence
def rotationEulerXYZ(phi, theta, psi):
    Rx = rotationX(phi)
    Ry = rotationY(theta)
    Rz = rotationZ(psi)
    R = np.matmul(Rx, np.matmul(Ry, Rz))
    return R

#degrees
phi_deg = -30.0    #roll
theta_deg = 65.0  #pitch
psi_deg = -45.0   #yaw

print("Euler Angles Degrees[{}, {}, {}]".format(phi_deg, theta_deg, psi_deg))

phi = degToRad(phi_deg)
theta = degToRad(theta_deg)
psi = degToRad(psi_deg)
print("Euler Angles Radians[{}, {}, {}]".format(phi, theta, psi))

Rxyz = rotationEulerXYZ(phi, theta, psi)
print("Euler Angles Rxyz\n {}".format(Rxyz))

# NOTE: Mathematically the function is written as an inverse (or arc tangent) but we lose 
# quadrant information. The return range of arctan is only QUADRANTS 1 and 4 or [-pi/2, pi/2]
# -a/b = a/-b = /a/b  -a/-b = a/b
# theta = arctan(x) = arctan(a/b) 
#              y
#              ^
#            2 | (1)
#         <----------> X
#            3 | (4)
#              v              
#
# When we implemment it in "code" we want to use a programming function commonly called "atan2"
# so that we can get a return range in ALL 4 QUADRANTS atan2(a,b) = arctan(a/b)  
#
# in C++ we implement:
# #include <cmath>
# double t = atan2(a,b)
# 
# in Python we implement:
# import numpy as np
# t = np.arctan2(a,b)

def eulerAnglesFromRxyz(Rxyz):
    phi = np.arctan2(Rxyz[1][2], Rxyz[2][2])
    theta = -np.arcsin(Rxyz[0][2])
    psi = np.arctan2(Rxyz[0][1], Rxyz[0][0])
    return (phi, theta, psi)

attitude = eulerAnglesFromRxyz(Rxyz)
phi2_deg = radToDeg(attitude[0])
theta2_deg = radToDeg(attitude[1])
psi2_deg = radToDeg(attitude[2])
print("Recovered Euler Angles Radians [{}, {}, {}]".format(attitude[0], attitude[1], attitude[2]))
print("Recovered Euler Angles Degrees [{}, {}, {}]".format(phi2_deg, theta2_deg, psi2_deg))