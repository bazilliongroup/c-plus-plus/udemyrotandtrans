import numpy as np
import matplotlib.pyplot as plt

def rotationX(theta):
    return np.array([[1,          0,           0],
                     [0,  np.cos(theta),  np.sin(theta)],
                     [0,  -np.sin(theta),  np.cos(theta)]])

def rotationY(theta):
    return np.array([[np.cos(theta),  0,  -np.sin(theta)],
                     [0,              1,       0],
                     [np.sin(theta),  0,  np.cos(theta)]])

def rotationZ(theta):
    return np.array([[np.cos(theta),  np.sin(theta),  0],
                     [-np.sin(theta),  np.cos(theta),  0],
                     [0,                    0,        1]])

# Here we rotate from the euler angle Rotations to XYZ Sequence
def dcmFromEulerXYZ(attitude_xyz):
    Rx = rotationX(attitude_xyz[0])
    Ry = rotationY(attitude_xyz[1])
    Rz = rotationZ(attitude_xyz[2])
    R = np.matmul(Rx, np.matmul(Ry, Rz))
    return R

def eulerAnglesFromDCM(Rxyz):
    phi = np.arctan2(Rxyz[1][2], Rxyz[2][2])
    theta = -np.arcsin(Rxyz[0][2])
    psi = np.arctan2(Rxyz[0][1], Rxyz[0][0])
    return np.array([phi, theta, psi])

def quaternionFromEulerXYZ(attitude_xyz):
    c1 = np.cos(0.5 * attitude_xyz[0])
    s1 = np.sin(0.5 * attitude_xyz[0])
    c2 = np.cos(0.5 * attitude_xyz[1])
    s2 = np.sin(0.5 * attitude_xyz[1])
    c3 = np.cos(0.5 * attitude_xyz[2])
    s3 = np.sin(0.5 * attitude_xyz[2])
    q0 = c1*c2*c3 + s1*s2*s3
    q1 = s1*c2*c3 - c1*s2*s3
    q2 = c1*s2*c3 + s1*c2*s3
    q3 = c1*c2*s3 - s1*s2*c3
    return np.array([q0,q1,q2,q3])        

def eulerXYZFromQuaternion(quat):
    q0 = quat[0]
    q1 = quat[1]
    q2 = quat[2]
    q3 = quat[3]
    r11 = q0*q0 + q1*q1 - q2*q2 - q3*q3
    r12 = 2.0 * (q1*q2 + q0*q3)
    r13 = 2.0 * (q1*q3 - q0*q2)
    r23 = 2.0 * (q2*q3 + q0*q1)
    r33 = q0*q0 - q1*q1 - q2*q2 + q3*q3
    phi = np.arctan2(r23, r33)
    theta = -np.arcsin(r13)
    psi = np.arctan2(r12, r11)
    return np.array([phi, theta, psi])

def quaternionNorm(quat):
    q0 = quat[0]
    q1 = quat[1]
    q2 = quat[2]
    q3 = quat[3]
    return np.sqrt(q0*q0 + q1+q1 + q2*q2 + q3*q3)

def quaternionNormalise(quat):
    norm = quaternionNorm(quat)
    return quat / norm

def radToDeg(rad):
    return np.array(rad) * (180.0 / np.pi)

def degToRad(deg):
    return np.array(deg) * (np.pi / 180.0)

def quaternionRates(quat, omega_body):
    q0 = quat[0]
    q1 = quat[1]
    q2 = quat[2]
    q3 = quat[3]
    W = np.array([[-q1, -q2, -q3], [q0, q3, -q2], [-q3, q0, q1], [q2, -q1, q0]])    
    return 0.5*np.matmul(W, omega_body)

def eulerAngleRatesXYZ(attitude, omega_body):
    phi   = attitude[0]
    theta = attitude[1]
    E = np.array([[1, np.tan(theta) * np.sin(phi), np.tan(theta) * np.cos(phi)],
                  [0, np.cos(phi),                 np.sin(phi)],
                  [0, np.sin(phi) / np.cos(theta), np.cos(phi) / np.cos(theta)]])
    return np.matmul(E, omega_body)

def eulerIntegration(X, Xdot, dt):
    return X + Xdot * dt

attitude0 = degToRad([0, 0, 0])
#omega_body = degToRad([10, 5, -15])     #first case
#omega_body = degToRad([0.01, 10, 0])   #second case
omega_body = degToRad([30, 45, 60])    #third case

attitude_q = quaternionFromEulerXYZ(attitude0)
attitude_euler = attitude0

dt = 0.01    
time = []
phi_q = []
theta_q = []
psi_q = []
phi_euler = []
theta_euler = []
psi_euler = []
for t in np.arange(0, 20 + dt, dt):
    q_dot = quaternionRates(attitude_q, omega_body)
    attitude_q = eulerIntegration(attitude_q, q_dot, dt)
    attitude_q = quaternionNormalise(attitude_q)
    attitude_q_euler = eulerXYZFromQuaternion(attitude_q)    

    euler_dot = eulerAngleRatesXYZ(attitude_euler, omega_body)
    attitude_euler = eulerIntegration(attitude_euler, euler_dot, dt)

    time.append(t)
    phi_q.append(radToDeg(attitude_q_euler[0]))
    theta_q.append(radToDeg(attitude_q_euler[1]))
    psi_q.append(radToDeg(attitude_q_euler[2]))
    phi_euler.append(radToDeg(attitude_euler[0]))
    theta_euler.append(radToDeg(attitude_euler[1]))
    psi_euler.append(radToDeg(attitude_euler[2]))    

plt.plot(time, phi_q, label="Roll (Quaternion)")
plt.plot(time, theta_q, label="Pitch (Quaternion)")
plt.plot(time, psi_q, label="Yaw (Quaternion)")
plt.plot(time, phi_euler, label="Roll (Euler)")
plt.plot(time, theta_euler, label="Pitch (Euler)")
plt.plot(time, psi_euler, label="Yaw (Euler)")
plt.legend()
plt.savefig("Exercise3Quaternions.png")

#RESULTS: The first case you see how the quaternions stay within a certain limit of 180 degrees 
# while the euler angles keep going on and on. In the second case the quaternions stayed within +/- 180 degrees while the
# euler angles went above 300. Third case, roll and yaw for euler went crazy while the quaternions stayed within the limits.
    

