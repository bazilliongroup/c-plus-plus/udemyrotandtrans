import numpy as np

def rotationX(theta):
    return np.array([[1,          0,           0],
                     [0,  np.cos(theta),  np.sin(theta)],
                     [0,  -np.sin(theta),  np.cos(theta)]])

def rotationY(theta):
    return np.array([[np.cos(theta),  0,  -np.sin(theta)],
                     [0,              1,       0],
                     [np.sin(theta),  0,  np.cos(theta)]])

def rotationZ(theta):
    return np.array([[np.cos(theta),  np.sin(theta),  0],
                     [-np.sin(theta),  np.cos(theta),  0],
                     [0,                    0,        1]])

def radToDeg(rad):
    return rad * (180.0 / np.pi)

def degToRad(deg):
    return deg * (np.pi / 180.0)

# Here we rotate from the euler angle Rotations to XYZ Sequence
def rotationEulerXYZ(phi, theta, psi):
    Rx = rotationX(phi)
    Ry = rotationY(theta)
    Rz = rotationZ(psi)
    R = np.matmul(Rx, np.matmul(Ry, Rz))
    return R

def rotationEulerZXZ(phi, theta, psi):
    Rz2 = rotationZ(phi)
    Rx = rotationX(theta)
    Rz1 = rotationZ(psi)
    R = np.matmul(Rz2, np.matmul(Rx, Rz1))
    return R

# Euler Angles Rotation to XYZ sequence
def eulerAnglesFromRxyz(Rxyz):
    phi = np.arctan2(Rxyz[1][2], Rxyz[2][2])
    theta = -np.arcsin(Rxyz[0][2])
    psi = np.arctan2(Rxyz[0][1], Rxyz[0][0])
    return (phi, theta, psi)

# Euler Angles Rotation to ZXZ sequence
def eulerAnglesFromRzxz(Rzxz):
    phi = np.arctan2(Rzxz[0][2], Rzxz[1][2])
    theta = np.arccos(Rzxz[2][2])
    psi = np.arctan2(Rzxz[2][0], -Rzxz[2][1])
    return (phi, theta, psi)

# Algebric Euler Angles XYZ sequence to ZXZ sequence Rxyz -> Rzxz
def eulerAngleSequenceXYZToZXZ(phi_xyz, theta_xyz, psi_xyz):
    phi_zxz   = np.arctan2(-np.sin(theta_xyz), np.sin(phi_xyz) * np.cos(theta_xyz))
    theta_zxz = np.arccos(np.cos(phi_xyz) * np.cos(theta_xyz))
    a =  np.cos(phi_xyz) * np.sin(theta_xyz) * np.cos(psi_xyz) + np.sin(phi_xyz) * np.sin(psi_xyz)
    b = -np.cos(phi_xyz) * np.sin(theta_xyz) * np.sin(psi_xyz) + np.sin(phi_xyz) * np.cos(psi_xyz)
    psi_zxz = np.arctan2(a, b)
    return (phi_zxz, theta_zxz, psi_zxz)

# degrees
phi_xyz_deg = -30.0    #roll
theta_xyz_deg = 65.0  #pitch
psi_xyz_deg = -45.0   #yaw
print("Euler Angles XYZ Degrees[{}, {}, {}]".format(phi_xyz_deg, theta_xyz_deg, psi_xyz_deg))

# radians
phi_xyz_rad = degToRad(phi_xyz_deg)
theta_xyz_rad = degToRad(theta_xyz_deg)
psi_xyz_rad = degToRad(psi_xyz_deg)
print("Euler Angles XYZ Radians[{}, {}, {}]".format(phi_xyz_rad, theta_xyz_rad, psi_xyz_rad))

# Generate the rotation Matrix from the XYZ sequence
# then generate the euler angles from the rotation matrix
Rxyz = rotationEulerXYZ(phi_xyz_rad, theta_xyz_rad, psi_xyz_rad)
attitude_zxz = eulerAnglesFromRzxz(Rxyz)

phi2_zxz_deg = radToDeg(attitude_zxz[0])
theta2_zxz_deg = radToDeg(attitude_zxz[1])
psi2_zxz_deg = radToDeg(attitude_zxz[2])
print("Euler Angles ZXZ Degrees [{}, {}, {}]".format(phi2_zxz_deg, theta2_zxz_deg, psi2_zxz_deg))
print("Euler Angles ZXZ Radians [{}, {}, {}]".format(attitude_zxz[0], attitude_zxz[1], attitude_zxz[2]))

attitude_zxz2 = eulerAngleSequenceXYZToZXZ(phi_xyz_rad, theta_xyz_rad, psi_xyz_rad)
phi2_zxz_deg2 = radToDeg(attitude_zxz2[0])
theta2_zxz_deg2 = radToDeg(attitude_zxz2[1])
psi2_zxz_deg2 = radToDeg(attitude_zxz2[2])
print("Algebric Euler Angles ZXZ Degrees [{}, {}, {}]".format(phi2_zxz_deg2, theta2_zxz_deg2, psi2_zxz_deg2))
print("Algebric Euler Angles ZXZ Radians [{}, {}, {}]".format(attitude_zxz2[0], attitude_zxz2[1], attitude_zxz2[2]))