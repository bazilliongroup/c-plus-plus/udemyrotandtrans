This folder we talk about: 

1) static libraires
2) sub-directories (levels)

// To build cmake use this command: 
cmake -S . -B AML/build/
make -C AML/build

//To clean:
cd AML/build
[type] make clean

//to buid all files go to: 
cd AML/build
[type] make

you should get this:
$ make
[  6%] Building CXX object AML/CMakeFiles/AML.dir/AMLVector3.cpp.o
[ 13%] Building CXX object AML/CMakeFiles/AML.dir/AMLMatrix33.cpp.o
[ 20%] Building CXX object AML/CMakeFiles/AML.dir/AMLDCM.cpp.o
[ 26%] Building CXX object AML/CMakeFiles/AML.dir/AMLEulerAngles.cpp.o
[ 33%] Building CXX object AML/CMakeFiles/AML.dir/AMLQuaternion.cpp.o
[ 40%] Linking CXX static library libAML.a
[ 40%] Built target AML
[ 46%] Building CXX object example/CMakeFiles/AML_Example.dir/main.cpp.o
[ 53%] Linking CXX executable AML_Example.exe
[ 53%] Built target AML_Example
[ 60%] Building CXX object test/CMakeFiles/AML_Test.dir/main.cpp.o
[ 66%] Building CXX object test/CMakeFiles/AML_Test.dir/AMLVector3Test.cpp.o
[ 73%] Building CXX object test/CMakeFiles/AML_Test.dir/AMLMatrix33Test.cpp.o
[ 80%] Building CXX object test/CMakeFiles/AML_Test.dir/AMLDCMTest.cpp.o
[ 86%] Building CXX object test/CMakeFiles/AML_Test.dir/AMLEulerAnglesTest.cpp.o
[ 93%] Building CXX object test/CMakeFiles/AML_Test.dir/AMLQuaternionTest.cpp.o
[100%] Linking CXX executable AML_Test.exe


//To run it we do this:
./AML/build/<executable>
