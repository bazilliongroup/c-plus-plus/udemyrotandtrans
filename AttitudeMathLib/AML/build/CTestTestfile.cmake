# CMake generated Testfile for 
# Source directory: /cygdrive/d/My code/C++/UdemyRotAndTrans/AttitudeMathLib
# Build directory: /cygdrive/d/My code/C++/UdemyRotAndTrans/AttitudeMathLib/AML/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test_all "test/AML_Test")
set_tests_properties(test_all PROPERTIES  _BACKTRACE_TRIPLES "/cygdrive/d/My code/C++/UdemyRotAndTrans/AttitudeMathLib/CMakeLists.txt;11;add_test;/cygdrive/d/My code/C++/UdemyRotAndTrans/AttitudeMathLib/CMakeLists.txt;0;")
subdirs("AML")
subdirs("example")
subdirs("test")
