// System Includes
#include <string>
#include <iostream>
#include <cmath>

// Program Includes
#include "AML.h"

using namespace AML;
using namespace std;

int main(int argc, char **argv)
{
    //Quaternion Class Example
    double deg2rad = M_PI / 180.0;
    EulerAngles angles1(10*deg2rad, -20*deg2rad, 15*deg2rad);
    EulerAngles angles2(40*deg2rad, -60*deg2rad, 135*deg2rad);

    Quaternion quat1 = eulerAngles2Quat(angles1);
    Quaternion quat2 = eulerAngles2Quat(angles2);

    std::cout << quat1 << std::endl;
    std::cout << quat2 << std::endl;

    Quaternion quat_interp = slerpInterpolate(quat1, quat2, 0.5);
    std::cout << "\nslerpInterplation angle1, angle2, 0.5" << std::endl;
    std::cout << quat_interp << std::endl;

    EulerAngles angles_interp = linearInterpolate(angles1, angles2, 0.5);
    std::cout << "\nangle1 in radians 10 deg, -20 deg, 15 deg" << std::endl;
    std::cout << angles1 << std::endl;
    std::cout << "\nangle2 in radians 40 deg, -60 deg, 135 deg" << std::endl;
    std::cout << angles2 << std::endl;

    std::cout << "\nlinearInterpolate angle1, angle2, 0.5" << std::endl;
    std::cout << angles_interp << std::endl;

    std::cout << "\nquaternions to euler angles" << std::endl;
    std::cout << quat2EulerAngles(quat_interp) << std::endl;

    //Euler Class Examples
    // EulerAngles angles(0.1, -0.3, -0.5);
    // Matrix33 dcm = eulerAngles2DCM(angles);

    // cout << "Initial angles in radians" << endl;
    // cout << angles << endl;
    // cout << "Euler angles to DCM (Discrete Cosine Matrix) angles" << endl;
    // cout << dcm << endl;

    // EulerAngles recovered_angles = dcm2EulerAngles(dcm);
    // cout << "Recovered angles" << endl;
    // cout << recovered_angles << endl;

    //DCM class examples
    // Matrix33 R  = Matrix33::identity();
    // for (unsigned int i = 0; i < 100; i++)
    // {
    //     Matrix33 Rdot = dcmKinematicRates_BodyRates(R, Vector3(1, 0.0, 0.0));
    //     R = integrateDCM(R, Rdot, 0.01);
    //     std::cout << R << std::endl;
    // }

    //Vector3, Matrix33 examples
    //

    // double data[3][3] = {{1.2, 3.2, 0.5}, {0.1, 1.0, 6.0}, {-4.1, 6.0, 8.0}};
    // Matrix33 Mat1 = Matrix33(data);
    // Vector3 v1 = Vector3(0,1,0);
    // Vector3 v2 = Mat1 * v1;
    // cout << v1 << endl;
    // cout << Mat1 << endl;
    // cout << v2 << endl;

    // Vector3 v1(1.0, 2.0, 3.0);
    // Vector3 v2(4.0, 5.0, 6.0);
    // Vector3 v3 = v1 + v2;
    // Vector3 v4 = v3 * v1;
    // cout << v1 << endl;
    // cout << v2 << endl;
    // cout << v3 << endl;
    // cout << v4 << endl;

    return 0;
}