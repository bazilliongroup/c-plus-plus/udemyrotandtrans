#include "catch/catch.hpp"
#include "AML.h"

using namespace AML;

TEST_CASE("Euler Angle Constructors", "[EulerAngles]")
{
    // Case 1
    EulerAngles angles;
    CHECK(angles.phi == 0.0);
    CHECK(angles.theta == 0.0);
    CHECK(angles.psi == 0.0);
    CHECK(angles.getEulerSequence() == EulerAngles::EulerSequence::XYZ);

    // Case 2
    angles = EulerAngles(1.0, 2.0, 3.0, EulerAngles::EulerSequence::ZXZ);
    CHECK(angles.phi == 1.0);
    CHECK(angles.theta == 2.0);
    CHECK(angles.psi == 3.0);
    CHECK(angles.getEulerSequence() == EulerAngles::EulerSequence::ZXZ);

}