#include "catch/catch.hpp"
#include "AML.h"

using namespace AML;

TEST_CASE("Matrix33 Constructors", "[Matrix33]")
{
    //Case 1
    Matrix33 m5;
    CHECK(m5.m11 == 0.0);
    CHECK(m5.m12 == 0.0);
    CHECK(m5.m13 == 0.0);
    CHECK(m5.m21 == 0.0);
    CHECK(m5.m22 == 0.0);
    CHECK(m5.m23 == 0.0);
    CHECK(m5.m31 == 0.0);
    CHECK(m5.m32 == 0.0);
    CHECK(m5.m33 == 0.0);     
    //Case 2
    Matrix33 m6 = Matrix33(5.0);
    CHECK(m6.m11 == 5.0);
    CHECK(m6.m12 == 5.0);
    CHECK(m6.m13 == 5.0);
    CHECK(m6.m21 == 5.0);
    CHECK(m6.m22 == 5.0);
    CHECK(m6.m23 == 5.0);
    CHECK(m6.m31 == 5.0);
    CHECK(m6.m32 == 5.0);
    CHECK(m6.m33 == 5.0);
    //Case 3
    double case3Data[9] = {5,5,5,5,5,5,5,5,5};
    Matrix33 m7 = Matrix33(case3Data);
    CHECK(m7.m11 == 5.0);
    CHECK(m7.m12 == 5.0);
    CHECK(m7.m13 == 5.0);
    CHECK(m7.m21 == 5.0);
    CHECK(m7.m22 == 5.0);
    CHECK(m7.m23 == 5.0);
    CHECK(m7.m31 == 5.0);
    CHECK(m7.m32 == 5.0);
    CHECK(m7.m33 == 5.0);    
    //Case 4
    double case4Data[3][3] = {{5,5,5},{5,5,5},{5,5,5}};
    Matrix33 m8 = Matrix33(case4Data);
    CHECK(m8.m11 == 5.0);
    CHECK(m8.m12 == 5.0);
    CHECK(m8.m13 == 5.0);
    CHECK(m8.m21 == 5.0);
    CHECK(m8.m22 == 5.0);
    CHECK(m8.m23 == 5.0);
    CHECK(m8.m31 == 5.0);
    CHECK(m8.m32 == 5.0);
    CHECK(m8.m33 == 5.0); 
}