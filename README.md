# UdemyRotAndTrans

Udemy Rotation and Transformation Project. The code is fron the udemy course:
https://www.udemy.com/course/complete-guide-to-rotations-and-transformations/learn/lecture/19926622?start=60#overview

by Steven Dumble
Aerospace Engineer, Phd, Teacher

## WHAT THE LIBRARY HAS AND DOESN'T HAVE
```
1) Uses C++ 11
2) Modular
3) Usable on Embedded platforms (arduino, microprocessors, etc...)
3a) For example to use on an embedded flight control System (e.g UAV quadcopter drones)
3b) No Virtual clases/inheritence, No dynamic memory allocation (Typcal requirements for safety critical control code) 
``` 

## Usage Structure
```
Classes to represent the basic linear math elements:
() Vector3
() Matrix33
Classes to represrnt the attitude representations:
() DCM -- Discrete cosine Matrix
() EulerAngles
() Quaternions
Easy Class Construction
  Vector3 v1(1.0, 2.0, 3.0);
  Matrix33 dcm = DCM::rotationX(1.2);
Easy Element Access
  double x = v1.x;
  double y = v1.data[1];
  double m11 = dcm.m11;
  double m12 = dcm.data[0][1];
Classes to have overloaded mathmathematical operators (i.e +,-,-,/,*)
  Vector3 v3 = v1 * v2;
  Matrix33 dcm2 = DCM::rotationX(0.1) * DCM::rotationY(2.5);
Free funciotns for special oprations
  double d = dot(v1, v2);
  Vector3 v3 = cross(v1, v2);
Vector transformation are handled automatically
  Vector3 v2_dcm = dcm * v1;
  Vector3 v2_quat = quat * v1;
  Vector3 v2_euler = euler * v1;
Conversion functions between Classes
  Matrix33 dcm = eulerAngles2DCM(angles);
  eulerAngles angles = dcm2EulerAngles(dcm, EulerAngles::EulerSequence::XYZ);
  Quaternion q = eulerAngles2Quat(angles);
```

## FINAL PROJECT STRUCTURE OVERVIEW
```
AttitudeMathLib
---CMakeList.txt
---AML                      (Main Library)
----|---CMakeLists.txt
----|---AML.h
----|---AMLVector3.h
----|---AMLVector3.cpp
----|---...
---test                     (Unit tests)
----|---CMakeList.txt
----|---main.cpp
----|---AMLVector3Test.cpp
----|---...
----|---catch       (catch2 test framework library)
----------|---catch.hpp
---example                  (Example Usage)
-----|---CMakeList.txt
-----|---main.cpp
```

## WHAT IS THE CATCH2 TESTING FRAMEWORK
```
Catch2 is mainly a unit testing framework for C++, but it also provides basic micro-benchmarking features, and simple BDD macros.

Catch2's main advantage is that using it is both simple and natural. Tests autoregister themselves and do not have to be named
with valid identifiers, assertions look like normal C++ code, and sections provide a nice way to share set-up and tear-down
code in tests.
```
https://github.com/catchorg/Catch2

## GETTING STARTED
```
To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!
```

## GETTING LIBRARY CODE 
```
make sure you are in "main"
git clone https://gitlab.com/bazilliongroup/c-plus-plus/udemyrotandtrans.git
cd existing_repo
```

## GETTING CODE IF YOU CAN'T USE SSH
```
Change from main to master.
download through *.zip. 
It will be a standalone of the code. 
```

## STEPS FOR SETUP
```
1) Download https://www.cygwin.com/
2) Install cygwin64.exe using the Installing Cygwin on the website. 
3) Include [CMAKE], [g++] in the installation
4) Type "./cmake --version", make sure that it shows, (the version may be different):
cmake version 3.21.4

CMake suite maintained and supported by Kitware (kitware.com/cmake).
5) Type " g++ --version", make sure that it this shows, (the version may be different):
g++ (GCC) 7.4.0
Copyright (C) 2017 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
6) Activate cygwin64 and go to the AttitudeMathLib/AML/build
7) To clean the code [type] make -c ((optional))
8) to build the library, example, and test [type] make

if [ cMake doesn't work: ] 
-bash: make: command not found
else
you will get this: 
$ make
[  6%] Building CXX object AML/CMakeFiles/AML.dir/AMLVector3.cpp.o
[ 13%] Building CXX object AML/CMakeFiles/AML.dir/AMLMatrix33.cpp.o
[ 20%] Building CXX object AML/CMakeFiles/AML.dir/AMLDCM.cpp.o
[ 26%] Building CXX object AML/CMakeFiles/AML.dir/AMLEulerAngles.cpp.o
[ 33%] Building CXX object AML/CMakeFiles/AML.dir/AMLQuaternion.cpp.o
[ 40%] Linking CXX static library libAML.a
[ 40%] Built target AML
[ 46%] Building CXX object example/CMakeFiles/AML_Example.dir/main.cpp.o
[ 53%] Linking CXX executable AML_Example.exe
[ 53%] Built target AML_Example
[ 60%] Building CXX object test/CMakeFiles/AML_Test.dir/main.cpp.o
[ 66%] Building CXX object test/CMakeFiles/AML_Test.dir/AMLVector3Test.cpp.o
[ 73%] Building CXX object test/CMakeFiles/AML_Test.dir/AMLMatrix33Test.cpp.o
[ 80%] Building CXX object test/CMakeFiles/AML_Test.dir/AMLDCMTest.cpp.o
[ 86%] Building CXX object test/CMakeFiles/AML_Test.dir/AMLEulerAnglesTest.cpp.o
[ 93%] Building CXX object test/CMakeFiles/AML_Test.dir/AMLQuaternionTest.cpp.o
[100%] Linking CXX executable AML_Test.exe
```

## TO EXECUTE THE TEST OR EXAMPLE CODE
```
//To run each executable do this:

cd AttitudeMathLib/AML/build/example/
./AML_Example.exe

cd AttitudeMathLib/AML/build/test/
./AML-Test.exe
```

## TO FIND THE LIBRARY
```
The library file is located at:
AttitudeMathLib/AML/build/AML/libAML.a
```

## README
```
Go to AttitudeMathLib and open up readme.txt
```

## Authors and acknowledgment
Ronald Bazillion git repositiory owner. 

Person who coded the library:
Steven Dumble -- Aerospace Engineer, Phd, Teacher

## License
For open source projects.
